FROM python:3.8-slim
#Use an existing linux os with python 3.8 installed
RUN apt-get update -y
#Update all the libraries in the os

RUN mkdir /app
COPY . /app
WORKDIR /app
#Create a directory to run our app in it
RUN pip install -r libraries.txt
#Install all python libraries need to run our apps
ADD . /app
EXPOSE 5000
ENV DEPLOY=config_dply.cfg
#Configure the environment
CMD gunicorn --bind 0.0.0.0:$PORT app:app
#Run gunicorn which is a python web server


